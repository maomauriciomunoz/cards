package com.amunoz.cards.credibanco.controller;


import com.amunoz.cards.credibanco.models.Persona;
import com.amunoz.cards.credibanco.models.Tarjeta;
import com.amunoz.cards.credibanco.models.Transaccion;
import com.amunoz.cards.credibanco.services.PersonaService;
import com.amunoz.cards.credibanco.services.TarjetaService;
import com.amunoz.cards.credibanco.services.TransaccionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
@CrossOrigin({"http://localhost:4200"})
public class TarjetaController {



    @Autowired
    private PersonaService service;

    @Autowired
    private TarjetaService tarjetaService;

    @Autowired
    private TransaccionService transaccionService;


    @GetMapping("/listar")
    public ResponseEntity<?> listarTarjeta() {
        return ResponseEntity.ok().body(tarjetaService.listar());
    }

    @GetMapping("/listar-transaccion")
    public ResponseEntity<?> listarTransaccion() {
        return ResponseEntity.ok().body(transaccionService.listarTrx());
    }


    //crea la tarjeta y se la asigna a un cliente por su id
    @PostMapping("/crear-tarjeta/{id}")
    ResponseEntity<?> crearTarjeta(@PathVariable Long id, @RequestBody Tarjeta tarjeta) {

        Optional<Persona> o = Optional.ofNullable(service.findById(id));

        if (o.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("cod 01. Fallido!");
        }

        Persona persona = o.get();

        tarjeta.setPersona(persona);
        int numVal = (int) (Math.random() * 99 + 1);
        tarjeta.setNumValidation(String.valueOf(numVal));
        tarjetaService.crear(tarjeta);

        return ResponseEntity.status(HttpStatus.CREATED).body("cod 00. Exito!");

    }

    //enrola tarjeta , se valida por si numValidation
    @PutMapping("/enrolar/{numValidation}")
    ResponseEntity<?> enrolarTarjeta(@RequestBody Tarjeta tarjeta, @PathVariable String numValidation) {


        Tarjeta t = tarjetaService.findByNumValidation(numValidation);
        if (t.getNumValidation().isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("cod. 001 elemento no encontrado");

        }
        if (!t.getNumValidation().equals(numValidation)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("cod. 002 Numero de validacion Invalido");
        }

        Tarjeta tarjetaDB = t;
        if (tarjetaDB.getNumValidation().equals(numValidation)) {
            tarjetaDB.setPAN(tarjeta.getPAN());
            tarjetaDB.setNumValidation(tarjeta.getNumValidation());
            tarjetaDB.setTipo(tarjeta.getTipo());
            tarjetaDB.setPersona(tarjeta.getPersona());
            tarjetaDB.setEstado("enrolada");
            tarjetaService.crear(tarjetaDB);
        }


        return ResponseEntity.status(HttpStatus.CREATED).body("cod. 00  Exito!");

    }


    //busca una tarjeta por su PAN
    @GetMapping("/listar-tarjeta/{pan}")
    public ResponseEntity<?> listarTarjeta(@PathVariable String pan) {

        //Tarjeta tarjeta = tarjetaService.findById(id);
/*        if (tarjeta.getId() == null){
            return ResponseEntity.notFound().build();
        }*/
        return ResponseEntity.ok(tarjetaService.findByPan(pan));
    }


    //elimina una tarjeta - se pasa pan y numValidation
    @DeleteMapping("/eliminar/{pan}/{numValidation}")
    public ResponseEntity<?> deleteTarjeta(@PathVariable String pan, @PathVariable String numValidation) {
        Tarjeta t = tarjetaService.findByPan(pan);

        if (!t.getNumValidation().equals(numValidation)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("cod 01. La tarjeta NO pudo ser eliminada!");


        }
        tarjetaService.delete(t);
        return ResponseEntity.status(HttpStatus.CREATED).body("cod 00.  tarjeta eliminada");
    }

    //crea la transaccion y se le asigna la tarjeta buscandola por su PAN
    @PostMapping("/crearTransaccion/{pan}")
    public ResponseEntity<String> crearTransaccion(@RequestBody Transaccion transaccion, @PathVariable String pan) {

        Tarjeta t = tarjetaService.findByPan(pan);

        if (t.getId() != null && t.getEstado().equals("enrolada")) {

            transaccion.setEstado("Aprobada");
            int numVal = (int) (Math.random() * 600000 + 1);
            transaccion.setNumReferencia(String.valueOf(numVal));
            transaccionService.crear(transaccion);
            return ResponseEntity.status(HttpStatus.CREATED).body("cod 00. compra exitosa!");

        }

        if (t.getId() != null && !t.getEstado().equals("enrolada")) {

            return ResponseEntity.status(HttpStatus.CREATED).body("cod 02. tarjeta NO enrolada!");

        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("cod 01. tarjeta NO existe!");
    }


    //encuentra una transaccion por su id
    @GetMapping("/encontrar-trx/{id}")
    public ResponseEntity<?> enconrtarTrx(@PathVariable Long id) {
        Optional<Transaccion> trx = transaccionService.encontrar(id);

        if (trx.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("cod 01. La tarjeta NO pudo ser eliminada!");

        }

        return ResponseEntity.status(HttpStatus.OK).body(trx);
    }

    //anula una transaccion encontrandola por su id
    @PutMapping("/anular/{id}")
    ResponseEntity<?> anular(@PathVariable Long id) {
        Optional<Transaccion> t = transaccionService.encontrar(id);
        Transaccion trx = t.get();
        long diff = Diferencia(trx);

        if (trx.getId() != null && diff <= 300000) {
            trx.setEstado("Anulada");
            transaccionService.crear(trx);
            return ResponseEntity.status(HttpStatus.CREATED).body("compra anulada!");

        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No se puede anular la transaccion!");
    }

    //valida que la compra  se haya realizado antes de 5 minutos
    private static long Diferencia(Transaccion trx) {
        Date d1 = trx.getCreateAt();
        Date d2 = new Date();
        long diff2 = d1.getTime() - d2.getTime();
        return diff2 * -1;
    }


}
