package com.amunoz.cards.credibanco.repositories;

import com.amunoz.cards.credibanco.models.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long> {
}
