package com.amunoz.cards.credibanco.repositories;

import com.amunoz.cards.credibanco.models.Transaccion;
import org.springframework.data.repository.CrudRepository;

public interface TransaccionRepository extends CrudRepository<Transaccion, Long> {
}
