package com.amunoz.cards.credibanco.repositories;

import com.amunoz.cards.credibanco.models.Tarjeta;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface TarjetaRepository extends CrudRepository<Tarjeta,Long> {

    @Query("select t from Tarjeta t where t.NumValidation like %?1%")
    public Tarjeta findByNumValidation(String term);


    @Query("select t from Tarjeta t where t.PAN like %?1%")
    public Tarjeta findByPan(String term);
}
