package com.amunoz.cards.credibanco.services;

import com.amunoz.cards.credibanco.models.Transaccion;
import com.amunoz.cards.credibanco.repositories.TransaccionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TransaccionServiceImpl implements TransaccionService{

    @Autowired
    TransaccionRepository Transaccionrepository;


    @Override
    public List<Transaccion> listarTrx() {
        return (List<Transaccion>) Transaccionrepository.findAll();
    }

    @Override
    public Transaccion crear(Transaccion transaccion) {
        return Transaccionrepository.save(transaccion);
    }



    @Override
    public Optional<Transaccion> encontrar(Long id) {
        return Transaccionrepository.findById(id);
    }

}
