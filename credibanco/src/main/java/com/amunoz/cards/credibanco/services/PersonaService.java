package com.amunoz.cards.credibanco.services;

import com.amunoz.cards.credibanco.models.Persona;

public interface PersonaService {

    public Persona findById(Long id);
}
