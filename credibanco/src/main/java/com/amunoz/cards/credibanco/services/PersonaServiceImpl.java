package com.amunoz.cards.credibanco.services;

import com.amunoz.cards.credibanco.models.Persona;
import com.amunoz.cards.credibanco.repositories.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class PersonaServiceImpl implements PersonaService{

    @Autowired
    private PersonaRepository repository;




    @Override
    @Transactional(readOnly = true)
    public Persona findById(Long id) {
        return repository.findById(id).orElse(null);
    }
}
