package com.amunoz.cards.credibanco.services;

import com.amunoz.cards.credibanco.models.Transaccion;

import java.util.List;
import java.util.Optional;

public interface TransaccionService {

    public List<Transaccion> listarTrx();


    public Transaccion crear(Transaccion transaccion);

    public Optional<Transaccion> encontrar(Long id);
}
