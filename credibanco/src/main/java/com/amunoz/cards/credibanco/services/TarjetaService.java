package com.amunoz.cards.credibanco.services;

import com.amunoz.cards.credibanco.models.Tarjeta;

import java.util.List;

public interface TarjetaService {

    List<Tarjeta> listar();

    Tarjeta crear(Tarjeta tarjeta);

    Tarjeta findByNumValidation(String term);

    Tarjeta findById(Long id);

    public Tarjeta findByPan(String term);


    void delete(Tarjeta tarjeta);

}
