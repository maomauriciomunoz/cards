package com.amunoz.cards.credibanco.services;

import com.amunoz.cards.credibanco.models.Tarjeta;
import com.amunoz.cards.credibanco.repositories.TarjetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TarjetaServiceImpl implements TarjetaService{

    @Autowired
    TarjetaRepository repository;


    @Override
    public List<Tarjeta> listar() {
        return (List<Tarjeta>) repository.findAll();
    }

    @Override
    @Transactional
    public Tarjeta crear(Tarjeta tarjeta) {
        return repository.save(tarjeta);
    }

    @Override
    public Tarjeta findByNumValidation(String term) {
        return repository.findByNumValidation(term);
    }

    @Override
    public Tarjeta findById(Long id) {
        return repository.findById(id).orElse(null);

    }

    @Override
    public Tarjeta findByPan(String term) {
        return repository.findByPan(term);
    }

    @Override
    public void delete(Tarjeta tarjeta) {
        repository.delete(tarjeta);
    }
}
